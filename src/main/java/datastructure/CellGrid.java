package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

	int rows;
	int cols;
	private CellState[][] grid;
	
    public CellGrid(int rows, int columns, CellState initialState) {
    	this.rows = rows;
    	this.cols = columns;
    	this.grid = new CellState[rows][columns];
    	
    	for (int i=0; i < rows; i++) {
    		for (int j=0; j < columns;j++) {
    			grid[i][j] = initialState;
    		}
    	}
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	this.grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
    	IGrid newGrid = new CellGrid(this.rows,this.cols,CellState.DEAD);
    	for (int i=0; i < this.rows; i++) {
    		for (int j=0; j < this.cols; j++) {
    			newGrid.set(i, j, this.get(i, j));
    		}
    	}
        return newGrid;
    }
    
}
