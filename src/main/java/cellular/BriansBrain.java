package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

	IGrid currentGeneration;
	
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}
	
	@Override
	public CellState getCellState(int row, int col) {
		return this.currentGeneration.get(row, col);
	}
	
	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int i=0; i < currentGeneration.numRows(); i++) {
			for (int j=0; j < this.currentGeneration.numColumns(); j++) {
				nextGeneration.set(i, j, this.getNextCell(i, j)); 
			}
		}
		this.currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		int n = this.countNeighbors(row, col, CellState.ALIVE);
		if (this.currentGeneration.get(row, col).equals(CellState.ALIVE)) {
			return CellState.DYING;
		} else if (this.currentGeneration.get(row, col).equals(CellState.DYING)) {
			return CellState.DEAD;
		} else {
			if (n == 2) {
				return CellState.ALIVE;
			}
			return CellState.DEAD;
		}
	}

	private int countNeighbors(int row, int col, CellState state) {
		int counter = 0;
		for (int i = row-1; i < row+2; i++) {
			for (int j = col-1; j < col+2; j++) {
				if (i == row && j == col) {
					continue;
				}
				if (i >= 0 && i < this.numberOfRows() && j >= 0 && j < this.numberOfColumns()) {
					if (this.currentGeneration.get(i, j).equals(state)) {
						counter++;
					}
				}
			}
		}
		return counter;
	}
	
	@Override
	public int numberOfRows() {
		return this.currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return this.currentGeneration.numColumns();
	}

	@Override
	public IGrid getGrid() {
		return this.currentGeneration;
	}

}
